var express = require('express');
var app = express();

const PORT = 80;
const HOST = '0.0.0.0';

app.get('/', function (req, res) {
   res.send('Hello World');
});

var server = app.listen(PORT, HOST);
   
console.log(`App listening at http://${HOST}:${PORT}`);
