resource "aws_instance" "example" {
  ami = var.my_ami
  instance_type = "t2.micro"

  security_groups = ["DMZ"]

  user_data = file("userdata.sh")
}
