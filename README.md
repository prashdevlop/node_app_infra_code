# Simple Node app with Hello World
```
steps:
1. Docker image created from Docker file using app.js 
2. app image pushed to dockerhub
sh build.sh
3. Infrastructure required to run application setup using Terraform along with deployment script
terraform apply
4. Testing application browser with http:<public-ip-of-ec2>
```
## Next steps
```
1. Providing resilliany and HA need to create cluster of ECS/EKS with replica counts
2. Enabling cloudwatch monitoring
3. Enabling CI/CD
```