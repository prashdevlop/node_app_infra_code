FROM node:14.10.1-alpine

# setting work directory
WORKDIR /usr/src/app

# Installing app dependencies
COPY package*.json ./

RUN npm install
RUN npm ci --only=production

COPY app.js .

EXPOSE 80
CMD [ "node", "app.js" ]
